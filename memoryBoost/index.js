const btn = document.getElementById("start");
const container = document.querySelector(".container");
const gameStatus = document.querySelector(".game-status");

//Global
var globalGameState = {
	isGameFinished: false,
	winner: "",
	level: 1,
};
//Game start on click
btn.addEventListener("click", startGame);

//Block render on window load
window.addEventListener("load", initGame);

function initGame() {
	for (let i = 0; i < 10; i++) {
		const item = document.createElement("div");
		item.classList.add("item", "blue");
		item.id = i + 1;
		container.appendChild(item);
	}
}

function startGame() {
	const compClickOrder = computerClick();
	simulateClick(compClickOrder, onSimulationFinish);

	function onSimulationFinish() {
		listenUserClick(compClickOrder, checkGameWinner);
	}
}

function checkGameWinner(computerClickOrder, userClickOrder) {
	/*
	TODO: WRITE YOUR GAME CHECK LOGIC HERE
	ADDITIONAL: GAME LEVEL + DIFFICULTY
	ADDITIONAL: GAME SAVING (REACH CURRENT LEVEL ON NEXT TIME VISIT)
	*/
	console.log("user: ", computerClickOrder, " computer: ", userClickOrder);
	gameStatus.textContent = "{Computer || You} win!";
}

function computerClick() {
	gameStatus.textContent = "Computer's Turn";
	let arr = [];
	for (let i = 0; i < 5; ++i) {
		let random = Math.ceil(Math.random() * 10);
		arr.push(random);
	}
	return arr;
}

function simulateClick(compClickOrder, finishHandler) {
	let counter = 0;
	let count = compClickOrder.length - 1;
	const handler = setInterval(() => {
		if (count < counter) {
			clearInterval(handler);
			finishHandler();
			return;
		}
		let item = document.getElementById(compClickOrder[counter]);

		item.classList.add("marx");
		setTimeout(() => {
			item.classList.remove("marx");
		}, 500);
		console.log("item id", item.id);
		counter++;
	}, 1000);
}

function listenUserClick(compClickOrder, checkGameWinner) {
	console.log("started listening");
	gameStatus.textContent = "Your Turn";
	container.classList.add("clickable");
	const computerClickCount = compClickOrder.length;
	let userClickOrder = [];

	function handleClick(event) {
		let itemId = event.target.id;
		userClickOrder.push(itemId);
		if (userClickOrder.length == computerClickCount) {
			container.removeEventListener("click", handleClick);
			checkGameWinner(compClickOrder, userClickOrder);
			container.classList.remove("clickable");
			return;
		}
	}

	container.addEventListener("click", handleClick);
}
